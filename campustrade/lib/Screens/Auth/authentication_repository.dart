import 'package:campustrade/Screens/Login/login_controller.dart';
import 'package:campustrade/Screens/Mypage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:campustrade/Screens/Login/login_screens.dart';
import 'exceptions/signup_email_passwordd_failure.dart';
import 'exceptions/login_with_email_and_pssword_failure.dart';

class AuthenticationRepository extends GetxController {
  static AuthenticationRepository get instance => Get.find();
  final _auth = FirebaseAuth.instance;
  late final Rx<User?> firebaseUser;
  int _success = 1;
  @override
  void onReady() {
    firebaseUser = Rx<User?>(_auth.currentUser);
    firebaseUser.bindStream(_auth.userChanges());
    ever(firebaseUser, _setInitialScreen);
  }

  _setInitialScreen(User? user) {
    // Primera pagina si no esta logueado segunda lo redirecciona a  la pagina de ventas ya logueado

    user == null
        ? Get.offAll(() => const LoginScreen())
        : Get.offAll(() => MyPage());
  }

  Future<String?> createUserWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential user_id = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User? id = user_id.user;
      if (id != null) {
        String cambios = id.uid;
        return cambios;
      }
      firebaseUser.value != null
          ? Get.offAll(() => MyPage())
          : Get.to(() => const LoginScreen());
    } on FirebaseAuthException catch (e) {
      final ex = SignUpWithEmailAndPasswordFailure.code(e.code);
      return ex.message;
    } catch (_) {
      const ex = SignUpWithEmailAndPasswordFailure();
      return ex.message;
    }
    return null;
  }

  Future<String?> loginWithEmailAndPassword(
      String email, String password) async {
    try {
      final UserCredential = (await _auth.signInWithEmailAndPassword(
          email: email, password: password));
      if (UserCredential != null) {
        return "true";
      }
    } on FirebaseAuthException catch (e) {
      final ex = LogInWithEmailAndPasswordFailure.fromCode(e.code);
      return ex.message;
    } catch (_) {
      const ex = LogInWithEmailAndPasswordFailure();
      return ex.message;
    }
    return "false";
  }

  Future<void> logout() async => await _auth.signOut();
}

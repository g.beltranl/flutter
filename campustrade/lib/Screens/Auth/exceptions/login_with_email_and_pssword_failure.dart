class LogInWithEmailAndPasswordFailure {
  final String message;
  const LogInWithEmailAndPasswordFailure([this.message = "An Unknow error ocurred"]);
  factory LogInWithEmailAndPasswordFailure.fromCode(String code) {
    switch (code) {
      case 'user-not-found':
        return const LogInWithEmailAndPasswordFailure('No user found for that email');
      case 'wrong-password':
        return const LogInWithEmailAndPasswordFailure('Wrong password provided by for that user');
      default:
        return const LogInWithEmailAndPasswordFailure();
    }
  }
}
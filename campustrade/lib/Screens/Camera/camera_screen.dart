import 'package:campustrade/Services/camera_controller.dart';
import 'package:campustrade/Services/database.dart';
import 'package:flutter/material.dart';
import '../Publish/publish_screen.dart';

class CameraScreen extends StatelessWidget {
  final bool uploadImage;
  const CameraScreen({super.key, this.uploadImage = true});

  @override
  Widget build(BuildContext context) {
    final cameraController = ModalRoute.of(context)!.settings.arguments as CameraController;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 40.0,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 47),
            child: const StyledText(
              text: 'Use your camera',
              size: 52,
              color: Colors.black,
              centered: true,
            ),
          ),
          const Icon(
            Icons.camera_alt_rounded,
            size: 170,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 100, vertical: 47),
            child: const StyledText(
                text: 'Use your camera\nto take a\nclear picture!',
                size: 22,
                color: Colors.black,
                centered: true),
          ),
          StyledButton(
            leftPadding: 0,
            topPadding: 35,
            rightPadding: 0,
            bottomPadding: 35,
            text: 'Start scanning',
            color: Color(0xFFFB8500),
            textColor: Colors.white,
            onPressed: () async {

              String? filePath = await cameraController.takePicture();
              Navigator.pop(context);
              if (uploadImage) {
                String? imageURL =
                await DatabaseService().saveImage(filePath);
                cameraController.setImageURL(imageURL!);
              }
              //print(filePath);

            },
          )
        ],
      ),
    );

  }
}

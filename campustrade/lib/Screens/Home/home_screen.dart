import 'package:campustrade/Shared/CustomAppBar.dart';
import 'package:flutter/material.dart';
import 'package:campustrade/Screens/Transactions/transactions_screen.dart';
import 'package:campustrade/assets/products/product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:campustrade/Screens/Publish/publish_screen.dart';
import 'package:provider/provider.dart';

import '../../Services/camera_controller.dart';
import '../../Services/publish_controller.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> docIDs = [];

  void reseteo() {
    docIDs = [];
  }

  Future getDocId() async {
    await FirebaseFirestore.instance.collection('ProductsDB').get().then(
          (snapshot) => snapshot.docs.forEach(
            (document) {
              if (document.data()["publishDate"] != null) {
                DateTime fecha = DateTime.parse(document.data()["publishDate"]);
                DateTime ahora = DateTime.now();
                if (fecha.isBefore(ahora)) {
                  docIDs.add(document.reference.id);
                }
              } else {
                docIDs.add(document.reference.id);
              }
            },
          ),
        );
  }

  @override
  Widget build(BuildContext context) {
    reseteo();
    return MaterialApp(
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(60),
          child: AppBar(
            
            backgroundColor: const Color(0xFF8ECAE6),
            leadingWidth: 400,
            leading: Row(

              children: [
                const SizedBox(width: 20),
                Container(
                  color: Colors.white,
                  height: 40,
                  child: (Expanded(
                    child: IconButton(
                      onPressed: () {
                        showSearch(
                          context: context,
                          delegate: CustomSearchDelegate(),
                        );
                      },
                      icon: const Icon(Icons.search),
                      color: Colors.black,
                      iconSize: 27,
                    ),
                  )),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  color: Colors.white,
                  height: 40,
                  width: 300,
                  child: (TextButton(
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.grey,
                      textStyle: const TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      showSearch(
                        context: context,
                        delegate: CustomSearchDelegate(),
                      );
                    },
                    child: const Text('Search...'),
                  )),
                )
              ],
            ),
          ),
        ),
        body: FutureBuilder(
          future: getDocId(),
          builder: (context, snapshot) {
            return OrientationBuilder(
              builder: (context, orientation) {
                return GridView.count(
                  childAspectRatio: (3 / 4),
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                  crossAxisCount: orientation == Orientation.portrait ? 2 : 4,
                  children: <Widget>[
                    for (int i = 0; i < docIDs.length; i++)
                      _crearItem(docIDs[i]),
                  ],
                );
              },
            );
          },
        ),
        bottomNavigationBar: CustomAppBar(context, true),
      ),
    );
  }
}

class CustomSearchDelegate extends SearchDelegate {
  List<String> searchTerms = [];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<String> matchQuery = [];
    for (var term in searchTerms) {
      if (term.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(term);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> matchQuery = [];
    for (var term in searchTerms) {
      if (term.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(term);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
  }
}

FutureBuilder _crearItem(String documentID) {
  CollectionReference products =
      FirebaseFirestore.instance.collection('ProductsDB');

  return FutureBuilder<DocumentSnapshot>(
    future: products.doc(documentID).get(),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.done) {
        Map<String, dynamic> data =
            snapshot.data!.data() as Map<String, dynamic>;
        return Container(
          decoration: const BoxDecoration(
            color: Color(0xFFEBEBEB),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 4,
                offset: Offset(1, 4), // Shadow position
              ),
            ],
          ),
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Image(image: NetworkImage(data['image'])),
              ),
              const SizedBox(height: 5),
              SizedBox(
                width: 160,
                child: Text(
                  data['name'],
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  softWrap: true,
                ),
              ),
              const SizedBox(height: 3),
              Text(
                data['price'].toString(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 3),
              Text(data['tags']),
              const SizedBox(height: 3),
              Text('Stock: ' + data['stock'].toString()),
            ],
          ),
        );
      }
      return const Text('Loading...');
    },
  );
}

Stream<List<Product>> readProductos() => FirebaseFirestore.instance
    .collection('ProductosDB')
    .snapshots()
    .map((snapshot) =>
        snapshot.docs.map((doc) => Product.fromJson(doc.data())).toList());

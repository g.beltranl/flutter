import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:campustrade/Screens/Auth/authentication_repository.dart';
import 'package:campustrade/Screens/Mypage.dart';

class LoginController extends GetxController {
  bool userchange = false;
  static LoginController get instance => Get.find();
  final email = TextEditingController();
  final password = TextEditingController();

  Future<String?> loginUser(String email, String password) async {
    final log_in = await AuthenticationRepository.instance.loginWithEmailAndPassword(email, password);
    
    return log_in;
  }
}

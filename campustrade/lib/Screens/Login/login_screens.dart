import 'package:campustrade/Screens/mypage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:campustrade/Screens/SignUp/signUp_screen.dart';
import 'package:campustrade/Screens/Login/login_controller.dart';
import 'package:campustrade/Screens/Home/home_screen.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(LoginController());
    final _formKey = GlobalKey<FormState>();

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 65),
                child: Text(
                  'CampusTrade',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: Color(0xFF1e465d)),
                  textScaleFactor: 3,
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextField(
                  controller: controller.email,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      hintText: 'Enter valid email id as abc@gmail.com'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextField(
                  controller: controller.password,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Enter secure password'),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Container(
                  height: 50,
                  width: 250,
                  child: ElevatedButton(
                    child: Text('LOG IN'),
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xFFFB8500),
                    ),
                    onPressed: () async {
                      var log_in = await LoginController.instance.loginUser(
                          controller.email.text.trim(),
                          controller.password.text.trim());
                      if ("true" == log_in) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => HomeScreen()),
                        );
                      }
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Container(
                  height: 50,
                  width: 250,
                  child: ElevatedButton(
                    child: Text('SIGN UP'),
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xFFFFB703),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const signUpScreen()),
                      );
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Center(
                  child: Container(
                      width: 200,
                      height: 150,
                      /*decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0)),*/
                      child: Image.asset('lib/assets/images/logo.png')),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:campustrade/Services/publish_controller.dart';
import 'package:elegant_notification/elegant_notification.dart';
import 'package:elegant_notification/resources/arrays.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../Services/camera_controller.dart';

class PublishScreen extends StatefulWidget {
  PublishScreen({Key? key}) : super(key: key);
  CameraController cameraController =
      CameraController(); // Get current cameraController

  @override
  State<PublishScreen> createState() => _PublishScreenState();
}

class _PublishScreenState extends State<PublishScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<PublishController>(
      builder: (context, publishProvider, child) {
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 40.0,
              ),
            ),
            title: const StyledText(
              text: 'Publish',
              size: 26,
              color: Colors.black,
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          body: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Row(
                          // Row style
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Consumer<CameraController>(
                                builder: (context, provider, child) {
                              publishProvider.setImageUrl(provider.imageUrl);
                              return
                                  // Add photo button
                                  provider.filePath.isEmpty
                                      ? Container(
                                          //Button style
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: const Color(0xFFFFB703),
                                          ),
                                          width: 120,
                                          height: 120,
                                          margin: const EdgeInsets.all(18),

                                          //Button icon
                                          child: IconButton(
                                            onPressed: () {
                                              Navigator.of(context).pushNamed(
                                                  '/camera',
                                                  arguments: provider);
                                            },
                                            icon: const Icon(
                                              Icons.add_photo_alternate,
                                              size: 55.0,
                                            ),
                                          ),
                                        )
                                      : Padding(
                                          padding: const EdgeInsets.all(18.0),
                                          child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Container(
                                                width: 120,
                                                height: 120,
                                                child: Image.file(
                                                  File(provider.filePath),
                                                  fit: BoxFit.fill,
                                                ),
                                              )),
                                        );
                            }),

                            //Text fields
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    //Name label
                                    const StyledText(
                                      text: 'Name your product',
                                      size: 18,
                                      color: Colors.black,
                                    ),
                                    StyledTextField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter a name';
                                        }
                                        return null;
                                      },
                                      hintText: 'Short name',
                                      controller: publishProvider.productName,
                                      maxLength: 30,
                                    ),
                                    //Price label
                                    const StyledText(
                                      text: 'Give it a price',
                                      size: 18,
                                      color: Colors.black,
                                    ),
                                    StyledTextField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty || double.tryParse(value) == null) {
                                          return 'Please enter a valid price';
                                        }
                                        return null;
                                      },
                                      maxLength: 6,
                                      hintText: '\$ 10 000',
                                      controller: publishProvider.price,
                                      textInputType: TextInputType.number,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //Description label
                        const StyledText(
                          text: 'Say a little more about it',
                          size: 18,
                          color: Colors.black,
                        ),
                        StyledTextField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          maxLines: 10,
                          maxLength: 300,
                          hintText: 'Short description',
                          controller: publishProvider.description,
                        ),
                        //Condition label
                        const StyledText(
                          text: 'What\'s it\'s condition',
                          size: 18,
                          color: Colors.black,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //New button
                            StyledToggleButton(
                              leftPadding: 18,
                              topPadding: 18,
                              rightPadding: 4.5,
                              bottomPadding: 18,
                              text: 'New',
                              buttonIndex: 0,
                              controller: publishProvider,
                              type: 1,
                            ),

                            //Used button
                            StyledToggleButton(
                              leftPadding: 4.5,
                              topPadding: 18,
                              rightPadding: 18,
                              bottomPadding: 18,
                              text: 'Used',
                              buttonIndex: 1,
                              controller: publishProvider,
                              type: 1,
                            ),
                          ],
                        ),
                        //Specifications label
                        const StyledText(
                          text: 'Technical specifications',
                          size: 18,
                          color: Colors.black,
                        ),
                        StyledTextField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          maxLines: 5,
                          maxLength: 100,
                          hintText: 'Something technical',
                          controller: publishProvider.technicalSpecs,
                        ),
                        //Tags label
                        const StyledText(
                          text: 'Tags',
                          size: 18,
                          color: Colors.black,
                        ),
                        StyledTextField(
                          maxLength: 30,
                          hintText: '#Tags',
                          controller: publishProvider.tags,
                        ),
                        //Category label
                        const StyledText(
                          text: 'Category',
                          size: 18,
                          color: Colors.black,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //New button
                            StyledToggleButton(
                              leftPadding: 18,
                              topPadding: 0,
                              rightPadding: 5,
                              bottomPadding: 5,
                              text: 'Material',
                              buttonIndex: 0,
                              controller: publishProvider,
                              type: 2,
                            ),

                            //Used button
                            StyledToggleButton(
                              leftPadding: 5,
                              topPadding: 0,
                              rightPadding: 18,
                              bottomPadding: 5,
                              text: 'Product',
                              buttonIndex: 1,
                              controller: publishProvider,
                              type: 2,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //New button
                            StyledToggleButton(
                              leftPadding: 18,
                              topPadding: 0,
                              rightPadding: 5,
                              bottomPadding: 18,
                              text: 'Accessory',
                              buttonIndex: 2,
                              controller: publishProvider,
                              type: 2,
                            ),

                            //Used button
                            StyledToggleButton(
                              leftPadding: 5,
                              topPadding: 0,
                              rightPadding: 18,
                              bottomPadding: 18,
                              text: 'Other',
                              buttonIndex: 3,
                              controller: publishProvider,
                              type: 2,
                            ),
                          ],
                        ),
                        //Stock label
                        const StyledText(
                          text: 'Quantity',
                          size: 18,
                          color: Colors.black,
                        ),

                        StyledTextField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          maxLength: 4,
                          hintText: '1 - 100',
                          controller: publishProvider.stock,
                          textInputType: TextInputType.number,
                        ),
                        //Publish & program button
                        Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: const Color(0xFFFFB703),
                              ),
                              child: IconButton(
                                  onPressed: () async {
                                    DateTime? pickedDate = await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime.now(),
                                        //DateTime.now() - not to allow to choose before today.
                                        lastDate: DateTime(2100));

                                    if (pickedDate != null) {
                                      print(
                                          pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                                      String formattedDate =
                                          DateFormat('yyyy-MM-dd')
                                              .format(pickedDate);
                                      print(
                                          formattedDate); //formatted date output using intl package =>  2021-03-16
                                      publishProvider
                                          .setPublishDate(formattedDate);
                                    } else {}
                                  },
                                  icon: const Icon(Icons.access_time_rounded)),
                            ),
                            StyledButton(
                              height: 47,
                              leftPadding: 5,
                              topPadding: 18,
                              rightPadding: 18,
                              bottomPadding: 18,
                              text: "Publish",
                              color: Color(0xFFFB8500),
                              textColor: Colors.white,
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  print('valid');
                                  publishProvider.handleSubmit();
                                  Navigator.pop(context);
                                  ElegantNotification(
                                    title: const Text("Success"),
                                    description: const Text(
                                        "Your product was published"),
                                    icon: const Icon(
                                      Icons.check,
                                      color: Colors.green,
                                    ),
                                    radius: 10,
                                    showProgressIndicator: false,
                                    notificationPosition:
                                        NotificationPosition.bottomCenter,
                                    animation: AnimationType.fromBottom,
                                  ).show(context);
                                }
                                else{
                                  print("Not valid");
                                }
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class StyledTextField extends StatelessWidget {
  final String hintText;
  final TextEditingController? controller;
  final TextInputType? textInputType;
  final int maxLength;
  final int maxLines;
  final String? Function(dynamic value)? validator;

  const StyledTextField({
    super.key,
    required this.hintText,
    this.controller,
    this.textInputType,
    this.maxLength = 30,
    this.maxLines = 1,
    this.validator,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
      child: Row(
        children: [
          Expanded(
              child: TextFormField(
            validator: validator,
            maxLines: maxLines,
            minLines: 1,
            maxLength: maxLength,
            keyboardType: textInputType,
            controller: controller,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide.none,
                ),
                fillColor: const Color(0xFFd9d9d9),
                filled: true,
                hintText: hintText,
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: Color(0xFFFFB703),
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(10),
                )),
          )),
        ],
      ),
    );
  }
}

class StyledButton extends StatelessWidget {
  final double leftPadding;
  final double topPadding;
  final double rightPadding;
  final double bottomPadding;
  final String text;
  final Color color;
  final Color textColor;
  final Function? onPressed;
  final double? height;

  const StyledButton({
    super.key,
    this.leftPadding = 0,
    this.topPadding = 0,
    this.rightPadding = 0,
    this.bottomPadding = 0,
    required this.text,
    required this.color,
    required this.textColor,
    this.onPressed,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        height: height ?? null,
        margin: EdgeInsets.fromLTRB(
            leftPadding, topPadding, rightPadding, bottomPadding),
        child: ElevatedButton(
          onPressed: () {
            onPressed!();
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: color,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            shadowColor: const Color(0xFFD9D9D9),
          ),
          child: StyledText(
            text: text,
            size: 23,
            color: textColor,
          ),
        ),
      ),
    );
  }
}

class StyledToggleButton extends StatefulWidget {
  final double leftPadding;
  final double topPadding;
  final double rightPadding;
  final double bottomPadding;
  final String text;
  final int buttonIndex;
  final PublishController controller;
  final int type; // type 1: condition , type 2: category

  const StyledToggleButton({
    super.key,
    this.leftPadding = 0,
    this.topPadding = 0,
    this.rightPadding = 0,
    this.bottomPadding = 0,
    required this.text,
    required this.buttonIndex,
    required this.controller,
    required this.type,
  });

  @override
  State<StyledToggleButton> createState() => _StyledToggleButtonState();
}

class _StyledToggleButtonState extends State<StyledToggleButton> {
  late Color color;
  late Color textColor;

  @override
  Widget build(BuildContext context) {
    if (widget.controller.getState(widget.buttonIndex, widget.type)) {
      textColor = Colors.white;
      color = const Color(0xFFFFB703);
    } else {
      textColor = Colors.black;
      color = const Color(0xFFD9D9D9);
    }
    return Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.fromLTRB(widget.leftPadding, widget.topPadding,
            widget.rightPadding, widget.bottomPadding),
        child: ElevatedButton(
          onPressed: () {
            widget.controller.changeCondition(widget.buttonIndex, widget.type);
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: color,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              shadowColor: const Color(0xFFD9D9D9)),
          child: StyledText(
            text: widget.text,
            size: 23,
            color: textColor,
          ),
        ),
      ),
    );
  }
}

class StyledText extends StatelessWidget {
  final String text;
  final double size;
  final Color color;
  final bool centered;

  const StyledText(
      {super.key,
      required this.text,
      required this.size,
      required this.color,
      this.centered = false});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontFamily: 'InriaSans',
        fontWeight: FontWeight.bold,
        fontSize: size,
      ),
      textAlign: centered ? TextAlign.center : null,
    );
  }
}

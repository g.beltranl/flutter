import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:campustrade/Screens/Auth/authentication_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignUpController extends GetxController {
  static SignUpController get instance => Get.find();
  final email = TextEditingController();
  final password = TextEditingController();
  final confirmpassword = TextEditingController();
  final name = TextEditingController();
  final tag = TextEditingController();
  final image = TextEditingController();
  final dateInput = TextEditingController();
  final CollectionReference userCollection =FirebaseFirestore.instance.collection('users');
  Future updateDataUser(
      String name, String email, String tag, String? uid, String image) async {
        if (image=="")
        {
          image='https://firebasestorage.googleapis.com/v0/b/campustrade-6d7b6.appspot.com/o/images%2F1679695262233.jpeg?alt=media&token=fd2f1af8-bde6-4120-901d-8504499c5523';
        }
    return await userCollection
        .doc(uid)
        .set({'email': email, 'name': name, 'tag': tag, 'image': image});
  }

  Future<String?> registerUser(String email, String password, String name,
      String tag, String confirmpassword, String image) async {
    
    if (password != confirmpassword) {
      return "Passwords do not match";
    } else if (email.isEmpty || tag.isEmpty || name.isEmpty ||  password.isEmpty) {
      return "Fill all the fields";    
      } else {
      String? error = await AuthenticationRepository.instance
          .createUserWithEmailAndPassword(email, password) as String?;
      

      if (error == "Please enter a strong paswword" ||
          error == "Email is not valid" ||
          error == "An account is already use this email ." ||
          error == "Operation is not allowed" ||
          error == "This user has been disabled .") {
        return error;
      } else {
        updateDataUser(name, email, tag, error,image);
        return "Your account has been created";
      }
    }
  }


}

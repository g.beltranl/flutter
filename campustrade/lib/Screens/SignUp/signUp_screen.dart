import 'package:campustrade/Screens/SignUp/signUp_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:campustrade/Screens/Login/login_screens.dart';
import 'package:campustrade/Services/database.dart';
import 'package:campustrade/Services/camera_controller.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class signUpScreen extends StatefulWidget {
  const signUpScreen({super.key});

  @override
  State<signUpScreen> createState() => _signUpScreenState();
}

class _signUpScreenState extends State<signUpScreen> {
  String? selectedValue;
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(SignUpController());
    final _formKey = GlobalKey<FormState>();
    final List<String> items = [
      'Accesory',
      'Product',
      'Material',
      'Other',
    ];
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Color(0xFF1e465d))),
        leading: const BackButton(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Container(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    child: Image.asset('lib/assets/images/camera.PNG'),
                    style: ElevatedButton.styleFrom(
                      primary:Colors.white,           ),
                    onPressed: () async {
                      String? picturePath = await CameraController().takePicture();
                      picturePath ??= "";
                      String? imageURL =
                          await DatabaseService().saveImage(picturePath);
                      imageURL ??= "";
                      controller.image.text = imageURL;
                    },
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 15),child:
              Container(child:Align( alignment: Alignment.centerLeft , child:
              Text(
                'Name',
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black),
              ),
              ),
              ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: TextField(
                  controller: controller.name,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Name',
                      hintText: 'Enter your Name'),
                ),
              ),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 15),child:
              Container(child:Align( alignment: Alignment.centerLeft , child:
              Text(
                'Email',
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black),
              ),),),),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: TextField(
                  controller: controller.email,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      hintText: 'Enter valid email id as abc@uniandes.edu.co'),
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 15),child:
              Container(child:Align( alignment: Alignment.centerLeft , child:
              Text(
                'Tags',
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black),
              ),),),),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton2(
                    hint: Text(
                      'Select Item',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    items: items
                        .map((item) => DropdownMenuItem<String>(
                              value: item,
                              child: Text(
                                item,
                                style: const TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                            ))
                        .toList(),
                    value: selectedValue,
                    onChanged: (value) {
                      controller.tag.text = value!;
                      setState(() {
                        selectedValue = value as String;
                      });
                    },
                    buttonStyleData: const ButtonStyleData(
                      height: 40,
                      width: 140,
                    ),
                    menuItemStyleData: const MenuItemStyleData(
                      height: 40,
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 15),child:
              Container(child:Align( alignment: Alignment.centerLeft , child:
              Text(
                'Password',
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black),
              ),),),),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 15),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextField(
                  controller: controller.password,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Enter secure password'),
                ),
              ),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 15),child:
              Container(child:Align( alignment: Alignment.centerLeft , child:
              Text(
                'Confirm Password',
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black),
              ),),),),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 15),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: controller.confirmpassword,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Enter secure password'),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Container(
                  height: 50,
                  width: 250,
                  child: ElevatedButton(
                    child: Text('Confirm'),
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xFFFB8500),
                    ),
                    onPressed: () async {
                      var message = await SignUpController.instance
                          .registerUser(
                              controller.email.text.trim(),
                              controller.password.text.trim(),
                              controller.name.text.trim(),
                              controller.tag.text.trim(),
                              controller.confirmpassword.text.trim(),
                              controller.image.text.trim());
                      message ??= "";
                      controller.email.clear();
                      controller.password.clear();
                      controller.name.clear();
                      controller.confirmpassword.clear();
                      controller.image.clear();
                      String change = message;
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text('Sign Up'),
                          content: Text(change),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'OK'),
                              child: const Text('OK'),
                            )
                          ],
                        ),
                      );

                      /*Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()));*/
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


import 'package:campustrade/Screens/Home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:campustrade/Screens/Publish/publish_screen.dart';
import 'package:provider/provider.dart';

import '../../Services/camera_controller.dart';
import '../../Services/publish_controller.dart';
import '../../Shared/CustomAppBar.dart';

class TransactionsScreen extends StatefulWidget {
  const TransactionsScreen({super.key});

  @override
  _TransactionsScreenState createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xFFFB8500), Colors.white],
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(100),
            child: AppBar(
              backgroundColor: const Color(0xFFFB8500),
              leadingWidth: 320,
              toolbarHeight: 150,
              leading: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(width: 20),
                      const SizedBox(
                        height: 40,
                        child: (Expanded(
                          child: Icon(
                            Icons.person,
                            color: Colors.black54,
                            size: 40,
                          ),
                        )),
                      ),
                      const SizedBox(width: 20),
                      Container(
                        alignment: Alignment.centerLeft,
                        height: 40,
                        width: 200,
                        child: const Text(
                          'Transactions',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 28,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(width: 20),
                      Container(
                        decoration: const BoxDecoration(
                          border: Border(
                            top: BorderSide(),
                            left: BorderSide(),
                            bottom: BorderSide(),
                          ),
                        ),
                        height: 40,
                        child: Expanded(
                          child: IconButton(
                            onPressed: () {
                              showSearch(
                                context: context,
                                delegate: CustomSearchDelegate(),
                              );
                            },
                            icon: const Icon(Icons.search),
                            color: Colors.black54,
                            iconSize: 27,
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        decoration: const BoxDecoration(
                          border: Border(
                            top: BorderSide(),
                            right: BorderSide(),
                            bottom: BorderSide(),
                          ),
                        ),
                        height: 40,
                        width: 250,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.black54,
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            showSearch(
                              context: context,
                              delegate: CustomSearchDelegate(),
                            );
                          },
                          child: const Text('Search...'),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          body: OrientationBuilder(
            builder: (context, orientation) {
              return GridView.count(
                childAspectRatio: (322 / 152),
                primary: false,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                crossAxisCount: orientation == Orientation.portrait ? 1 : 2,
                children: <Widget>[
                  _crearItem(
                      'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
                      'PRODUCTO',
                      'COP XXX.XXX',
                      'Units: X'),
                  _crearItem(
                      'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
                      'PRODUCTO',
                      'COP XXX.XXX',
                      'Units: X'),
                  _crearItem(
                      'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
                      'PRODUCTO',
                      'COP XXX.XXX',
                      'Units: X'),
                  _crearItem(
                      'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
                      'PRODUCTO',
                      'COP XXX.XXX',
                      'Units: X'),
                ],
              );
            },
          ),
          bottomNavigationBar: CustomAppBar(context , false),
        ),
      ),
    );
  }
}



class CustomSearchDelegate extends SearchDelegate {
  List<String> searchTerms = [];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<String> matchQuery = [];
    for (var term in searchTerms) {
      if (term.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(term);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> matchQuery = [];
    for (var term in searchTerms) {
      if (term.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(term);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
  }
}

Route _createRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => const HomeScreen(),
    transitionDuration: const Duration(milliseconds: 500),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0.0, 1.0);
      const end = Offset.zero;
      const curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

Container _crearItem(imagen, producto, precio, unidades) {
  return Container(
    decoration: BoxDecoration(
      color: const Color(0xFFEBEBEB),
      boxShadow: const [
        BoxShadow(
          color: Colors.grey,
          blurRadius: 4,
          offset: Offset(1, 4), // Shadow position
        ),
      ],
      borderRadius: BorderRadius.circular(15),
    ),
    height: 200,
    width: 150,
    padding: const EdgeInsets.all(10.0),
    margin: const EdgeInsets.all(10.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 132,
          width: 132,
          child: Expanded(
            child: Image(
              image: NetworkImage(imagen),
              fit: BoxFit.fill,
            ),
          ),
        ),
        const SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 160,
                child: Text(
                  producto,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 21),
                  softWrap: true,
                ),
              ),
            ),
            const SizedBox(height: 9),
            Flexible(
              child: SizedBox(
                width: 160,
                child: Text(
                  precio,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 18),
                  softWrap: true,
                ),
              ),
            ),
            const SizedBox(height: 9),
            Flexible(
              child: SizedBox(
                width: 160,
                child: Text(
                  unidades,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 18),
                  softWrap: true,
                ),
              ),
            ),
          ],
        )
      ],
    ),
  );
}

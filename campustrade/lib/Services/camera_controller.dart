import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CameraController extends ChangeNotifier {
  String filePath = '';
  String imageUrl = '';

  Future<String> takePicture() async {
    ImagePicker imagePicker = ImagePicker();
    XFile? file = await imagePicker.pickImage(source: ImageSource.camera);
    filePath = file!.path;
    notifyListeners();
    return filePath;
  }

  void setImageURL(String imageURL) {
    imageUrl = imageURL;
    notifyListeners();
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class DatabaseService {
  Reference database = FirebaseStorage.instance.ref();
  final CollectionReference _productCollection =
      FirebaseFirestore.instance.collection('ProductsDB');

  Future publishProduct(
      String productName,
      double price,
      String description,
      String conditionUsed,
      String technicalSpecs,
      String tags,
      String? imageURL,
      String category,
      String? publishDate,
      int stock) async {

    return await _productCollection.doc().set({
      'name': productName,
      'price': price,
      'description': description,
      'condition': conditionUsed,
      'technicalSpecs': technicalSpecs,
      'tags': tags,
      'image': imageURL,
      'type': category,
      'publishDate': publishDate,
      'stock': stock,
    });
  }

  Future<String?> saveImage(String path) async {
    String uniqueName = DateTime.now().millisecondsSinceEpoch.toString();
    Reference imageDirectory = database.child('images');
    Reference imageToUpload = imageDirectory.child(uniqueName);

    try {
      await imageToUpload.putFile(File(path));
      return await imageToUpload.getDownloadURL();
    } catch (error) {
      error.printInfo();
    }
    return null;
  }
}

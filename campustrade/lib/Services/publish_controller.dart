import 'package:campustrade/Services/database.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PublishController extends ChangeNotifier {
  final productName = TextEditingController();
  final price = TextEditingController();
  final description = TextEditingController();
  final List<bool> selectedConditionButton = <bool>[false, true];
  final List<bool> selectedCategoryButton = <bool>[true, false, false, false];
  final technicalSpecs = TextEditingController();
  final tags = TextEditingController();
  String? localImageUrl;
  String? publishDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final stock = TextEditingController();

  void handleSubmit() async {
    String conditionUsed = getConditionString(1);
    String category = getCategoryString();
    await DatabaseService().publishProduct(
      productName.text.trim(),
      double.parse(price.text),
      description.text.trim(),
      conditionUsed,
      technicalSpecs.text.trim(),
      tags.text.trim(),
      localImageUrl,
      category,
      publishDate,
      int.parse(stock.text),
    );

  }

  void changeCondition(int index, int type) {
    if (!getState(index, type)) {
      switch (type) {
        case 1:
          {
            for (int i = 0; i < selectedConditionButton.length; i++) {
              selectedConditionButton[i] = !selectedConditionButton[i];
            }
            notifyListeners();
          }
          break;
        case 2:
          {
            for (int i = 0; i < selectedCategoryButton.length; i++) {
              selectedCategoryButton[i] = false;
            }
            selectedCategoryButton[index] = true;
            notifyListeners();
          }
          break;
      }
    }
  }

  bool getState(int index, int type) {
    switch (type) {
      case 1:
        {
          return selectedConditionButton[index];
        }
        break;
      case 2:
        {
          return selectedCategoryButton[index];
        }
        break;
    }
    return selectedConditionButton[index];
  }

  String getConditionString(int index) {
    return selectedConditionButton[index] ? 'Used' : 'New';
  }

  String getCategoryString() {
    return selectedCategoryButton[0]
        ? 'Material'
        : selectedCategoryButton[1]
            ? 'Product'
            : selectedCategoryButton[2]
                ? 'Accessory'
                : 'Other';
  }

  void setImageUrl(String imageUrl) {
    localImageUrl = imageUrl;
  }

  void setPublishDate(String formattedDate) {
    publishDate = formattedDate;
  }

}

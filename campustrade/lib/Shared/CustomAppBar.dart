import 'package:elegant_notification/elegant_notification.dart';
import 'package:elegant_notification/resources/arrays.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../Screens/Home/home_screen.dart';
import '../Screens/Publish/publish_screen.dart';
import '../Screens/Transactions/transactions_screen.dart';
import '../Services/camera_controller.dart';
import '../Services/publish_controller.dart';

Widget CustomAppBar (BuildContext bigContext, bool onHomeScreen) {
  return BottomAppBar(
    height: 60,
    child: Row(
      children: [
        Expanded(
          child: SizedBox(
            height: 60,
            child: Material(
              color: const Color(0xFF023047), // button color
              child: InkWell(
                splashColor: const Color(0xFFFB8500), // splash color
                onTap: () {
                  if (!onHomeScreen) {
                    Navigator.of(bigContext).pushReplacement(
                      PageRouteBuilder(
                        pageBuilder:
                            (context, animation, secondaryAnimation) =>
                        const HomeScreen(),
                        transitionDuration: const Duration(milliseconds: 500),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          const begin = Offset(0.0, 1.0);
                          const end = Offset.zero;
                          const curve = Curves.ease;

                          var tween = Tween(begin: begin, end: end)
                              .chain(CurveTween(curve: curve));

                          return SlideTransition(
                            position: animation.drive(tween),
                            child: child,
                          );
                        },
                      ),
                    );
                  }
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(
                      Icons.home,
                      color: Colors.white,
                    ), // icon
                    Text(
                      "Home",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ), // text
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            height: 60,
            child: Material(
              color: const Color(0xFF023047), // button color
              child: InkWell(
                splashColor: const Color(0xFFFB8500), // splash color
                onTap: () {}, // button pressed
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(
                      Icons.explore,
                      color: Colors.white,
                    ), // icon
                    Text(
                      "Explore",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ), // text
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            height: 60,
            child: Material(
              color: const Color(0xFFFB8500), // button color
              child: InkWell(
                splashColor: const Color(0xFF023047), // splash color
                onTap: () {
                  Navigator.of(bigContext).push(
                    PageRouteBuilder(
                      pageBuilder: (context, animation, secondaryAnimation) =>
                          MultiProvider(
                            providers: [
                              ChangeNotifierProvider(
                                create: (bigContext) => PublishController(),
                              ),
                              ChangeNotifierProvider(
                                  create: (bigContext) => CameraController())
                            ],
                            builder: (context, child) {
                              return PublishScreen();
                            },
                          ),
                      transitionDuration: const Duration(milliseconds: 500),
                      transitionsBuilder:
                          (context, animation, secondaryAnimation, child) {
                        const begin = Offset(0.0, 1.0);
                        const end = Offset.zero;
                        const curve = Curves.ease;

                        var tween = Tween(begin: begin, end: end)
                            .chain(CurveTween(curve: curve));

                        return SlideTransition(
                          position: animation.drive(tween),
                          child: child,
                        );
                      },
                    ),
                  );
                }, // button pressed
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(
                      Icons.add,
                      color: Color(0xFF023047),
                    ), // icon
                    Text(
                      "Publish",
                      selectionColor: Color(0xFF023047),
                    ), // text
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            height: 60,
            child: Material(
              color: const Color(0xFF023047), // button color
              child: InkWell(
                splashColor: const Color(0xFFFB8500), // splash color
                onTap: () {
                  if (onHomeScreen) {
                    Navigator.of(bigContext).pushReplacement(
                      PageRouteBuilder(
                        pageBuilder:
                            (context, animation, secondaryAnimation) =>
                        const TransactionsScreen(),
                        transitionDuration: const Duration(milliseconds: 500),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          const begin = Offset(0.0, 1.0);
                          const end = Offset.zero;
                          const curve = Curves.ease;

                          var tween = Tween(begin: begin, end: end)
                              .chain(CurveTween(curve: curve));

                          return SlideTransition(
                            position: animation.drive(tween),
                            child: child,
                          );
                        },
                      ),
                    );
                  }
                }, // button pressed
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ), // icon
                    Text(
                      "Transactions",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ), // text
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            height: 60,
            child: Material(
              color: const Color(0xFF023047), // button color
              child: InkWell(
                splashColor: const Color(0xFFFB8500), // splash color
                onTap: () {}, // button pressed
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(
                      Icons.person,
                      color: Colors.white,
                    ), // icon
                    Text(
                      "Profile",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ), // text
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );


}

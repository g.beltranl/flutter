import 'dart:core';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  String id;
  final String condition;
  final String description;
  final String image;
  final String name;
  final int price;
  final String tags;
  final String technicalSpecs;
  final String type;
  final int stock;
  final String publishDate;

  Product({
    this.id = '',
    required this.condition,
    required this.description,
    required this.image,
    required this.name,
    required this.price,
    required this.tags,
    required this.technicalSpecs,
    required this.type,
    required this.stock,
    required this.publishDate,
  });

  Map<String, dynamic> toJson() => {
        'id': id,
        'condition': condition,
        'description': description,
        'image': image,
        'name': name,
        'price': price,
        'tags': tags,
        'technicalSpecs': technicalSpecs,
        'type': type,
        'stock': stock,
        'publishDate': publishDate,
      };

  static Product fromJson(Map<String, dynamic> json) => Product(
        id: json['id'],
        condition: json['condition'],
        description: json['description'],
        image: json['image'],
        name: json['name'],
        price: json['price'],
        tags: json['tags'],
        technicalSpecs: json['technicalSpecs'],
        type: json['type'],
        stock: json['stock'],
        publishDate: json['publishDate'],
      );
}

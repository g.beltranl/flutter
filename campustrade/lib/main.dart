import 'package:campustrade/Screens/Auth/authentication_repository.dart';
import 'package:campustrade/Screens/Camera/camera_screen.dart';
import 'package:campustrade/Screens/Publish/publish_screen.dart';
import 'package:campustrade/Services/camera_controller.dart';
import 'package:campustrade/Services/publish_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'Screens/Login/login_screens.dart';
import 'package:flutter/services.dart';
import 'Screens/Home/home_screen.dart';
import 'Screens/Transactions/transactions_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'firebase_options.dart';
import 'package:get/get.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  await Firebase.initializeApp(options: DefaultFirebaseOptions.android)
      .then((value) => Get.put(AuthenticationRepository()));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(initialRoute: '/', routes: {
      '/': (_) => const LoginScreen(),
      '/camera': (_) => const CameraScreen(),
    });
  }
}
